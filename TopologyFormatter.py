import ipaddress
import json
from collections import deque


class Node:
    """
        Represents a node in the network topology.
        Attributes:
            id (str): The unique identifier of the node.
            name (str): The name of the node.
            labels (list): A list of labels associated with the node.
            resources (list): A list of resource IDs associated w        connected_nodes (list): A list of connected nodes.

        Methods:
            add_connection(node): Adds a connection to another node.
    """

    def __init__(self, node_data):
        self.id = node_data['id']
        self.name = node_data.get('name', '')
        self.labels = node_data.get('labels', [])
        self.resources = node_data.get('resources', [])
        self.connected_nodes = []

    def add_connection(self, node):
        """
            Adds a connection to another node.

            Args:
                node (Node): The node to connect to.
        """
        self.connected_nodes.append(node)


def create_network(json_data):
    """
        Creates a network (graph) of nodes from the provided JSON data.

        Args:
            json_data (list): A list of dictionaries representing the nodes.

        Returns:
            dict: A dictionary mapping node IDs to Node objects, representing the network.
    """

    nodes = {}

    for item in json_data:
        node = Node(item)
        if "machine" in node.labels or "trust_zone" in node.labels:
            nodes[node.id] = node

    for node in nodes.values():
        for resource_id in node.resources:
            if resource_id in nodes:
                node.add_connection(nodes[resource_id])
                nodes[resource_id].add_connection(node)

    return nodes


def convert_network_to_format(network):
    """
        Converts the network representation to a desired format.

        Args:
            network (dict): A dictionary mapping node IDs to Node objects, representing the network.

        Returns:
            dict: A dictionary containing the formatted network representation.
    """

    result = {"nodes": []}

    for node_id, node in network.items():

        if len(node.connected_nodes) <= 0:
            continue

        node_info = {
            "name": node.name,
            "id": node.id,
            "type": node.labels,
            "interfaces": [],
            "connects_to": []
        }

        # Gather information about interfaces
        for resource_id in node.resources:
            if resource_id in network:
                connected_node = network[resource_id]
                if "interface" in connected_node.labels:
                    interface_info = {
                        "name": connected_node.name,
                        "ip_address": None,  # Placeholder for IP address
                        "subnet_mask": None  # Placeholder for subnet mask
                    }
                    node_info["interfaces"].append(interface_info)

        # Gather routing table information
        # routing_table_info = []  # Placeholder for routing table information
        # node_info["routing_table"] = routing_table_info

        # Gather connected nodes information
        for connected_node in node.connected_nodes:
            node_info["connects_to"].append(connected_node.name)

        result["nodes"].append(node_info)

    return result


def assign_interfaces_and_ips(formatted_network):
    """
        Assigns IP addresses and subnet masks to the interfaces of nodes in the formatted network.

        Args:
            formatted_network (dict): A dictionary containing the formatted network representation.

        Returns:
            dict: The formatted network with assigned IP addresses and subnet masks.
    """

    subnet_base = ipaddress.IPv4Network('10.0.0.0/24')  # Početna podmreža za dodjelu IP adresa
    subnet_third_counter = 1

    for node in formatted_network["nodes"]:
        subnet_counter = 1
        if "trust_zone" in node.get("type"):
            node["interfaces"] = []  # Resetiranje liste sučelja za svaki čvor
            subnet = subnet_base.subnets(new_prefix=24).__next__()  # Stvaranje nove podmreže s prefixom 28
            for connection in node["connects_to"]:
                interface_name = "eth" + str(len(node["interfaces"]))

                # Dohvaćanje sljedeće dostupne IP adrese
                ip_address = subnet.network_address + subnet_counter

                # Dodavanje sučelja
                node["interfaces"].append({
                    "name": interface_name,
                    "ip_address": str(ip_address),
                    "subnet_mask": str(subnet.netmask)  # Subnet maska je ista za sve IP adrese u podmreži
                })

                # Pronalaženje čvora na drugom kraju konekcije i dodjeljivanje IP adrese
                for connected_node in formatted_network["nodes"]:
                    if connection == connected_node["name"]:
                        subnet_counter += 1
                        ip_address = subnet.network_address + subnet_counter
                        connected_node["interfaces"].append({
                            "name": "eth0",
                            "ip_address": str(ip_address),
                            "subnet_mask": str(subnet.netmask)  # Subnet maska je ista za sve IP adrese u podmreži
                        })
                        break

                # Povećanje brojača podmreža za sljedeći čvor
                subnet_counter += 1
            # subnet_base = subnet.network_address + ipaddress.IPv4Address('0.0.1.0')  # Postavljamo sljedeću podmrežu kao početnu za sljedeći čvor

            # Povećanje IP adrese za drugi oktet
            subnet_network_address = subnet.network_address
            second_octet = subnet_network_address.packed[1]
            subnet_network_address = ipaddress.ip_address(
                subnet_network_address.packed[0:2] + bytes(
                    [second_octet + subnet_third_counter]) + subnet_network_address.packed[3:])
            subnet_base = ipaddress.IPv4Network((subnet_network_address, 24))

            subnet_third_counter += 1

    return formatted_network


def find_node(topology, ip_address):
    for node in topology['nodes']:
        for interface in node['interfaces']:
            if interface['ip_address'] == ip_address:
                return node
    return None


def find_path(topology, start_node_name, end_node_name):
    # Extract nodes and connections
    nodes = {node['name']: node['connects_to'] for node in topology['nodes']}

    # Check if start_node and end_node are the same
    if start_node_name == end_node_name:
        return [start_node_name]

    # Initialize the queue with the start node and a path containing just the start node
    queue = deque([(start_node_name, [start_node_name])])
    # Set to keep track of visited nodes
    visited = set()
    visited.add(start_node_name)

    while queue:
        # Get the current node and path
        current_node, path = queue.popleft()

        # Iterate over the neighbors of the current node
        for neighbor in nodes.get(current_node, []):
            if neighbor not in visited:
                # Create a new path including the neighbor
                new_path = path + [neighbor]
                # If the neighbor is the end node, return the new path
                if neighbor == end_node_name:
                    return new_path
                # Mark the neighbor as visited and add it to the queue
                visited.add(neighbor)
                queue.append((neighbor, new_path))

    # Return an empty list if no path is found
    return []


def run_formatter(topology_file):
    """
        Main function that runs the network creation and formatting process.

        Args:
            topology_file (str): The name of the JSON file containing the topology data.

        Returns:
            dict: The formatted network with assigned IP addresses and subnet masks.
    """

    # Oznake koje su dopuštene
    # allowed_labels = ["machine", "trust_zone", "physical_zone", "external_zone"]

    # Učitavanje JSON datoteke
    # ccs_topology = topology_file.split("\\")[-1]
    print(f"\tCCS topology -> {topology_file}")
    with open(f'{topology_file}') as file:
        json_data = json.load(file)

    # Stvaranje višestruko povezane liste čvorova
    print("\tKreiranje mreže (spajanje čvorova)")
    network = create_network(json_data)

    # Pretvaranje mreže u željeni format
    print("\tConverting to format")
    formatted_network = convert_network_to_format(network)

    print("\tAsigning interfaces and IP addresses")
    formatted_network_with_interfaces_ips = assign_interfaces_and_ips(formatted_network)

    # Zapisivanje rezultata u datoteku
    with open(f'./Topology/{topology_file.split("Topology")[1][1:].split(".json")[0]}_formatted.json', 'w') as outfile:
        json.dump(formatted_network_with_interfaces_ips, outfile, indent=2)

    print(f'\tFormatted network successfully saved to {topology_file.split(".json")[0]}_formatted.json.')

    return formatted_network_with_interfaces_ips

# run_formatter("Global.json")
