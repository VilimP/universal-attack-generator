from scapy.all import *


class PcapMerger:
    """
        A class for merging multiple PCAP files into a single PCAP file.

        Attributes:
            directory (str): The directory path containing the PCAP files to be merged.
            pcap_files (list): A list of PCAP file paths.
            packets (list): A list of packets from the merged PCAP files.
    """

    def __init__(self, directory, normal_traffic_pcap):
        self.directory = directory
        self.pcap_files = []
        self.packets = []
        self.normal_traffic_packets = []
        self.normal_traffic_pcap = normal_traffic_pcap

    def read_pcap_files(self):
        """
            Searches for PCAP files ending with '_gen.pcap' in the specified directory
            and adds their paths to the pcap_files list.
        """

        print("\t[+] Searching for _gen.pcap files")
        for filename in os.listdir(self.directory):
            if filename.endswith('_gen.pcap'):
                file_path = os.path.join(self.directory, filename)
                self.pcap_files.append(file_path)
                print(f"\t\t{file_path.replace('\\', '/')}")

    def merge_pcap_files(self):
        """
            Reads the packets from each PCAP file in the pcap_files list and appends them
            to the packets list.
        """

        print("\t[+] Reading and merging .pcap files")
        for pcap_file in self.pcap_files:
            print(f"\t\t{pcap_file.replace('\\', '/')}", end="  ")
            packets = rdpcap(pcap_file)
            for packet in packets:
                self.packets.append(packet)
            print("\t\tDone!")

        # add normal traffic packets
        self.packets.extend(self.normal_traffic_packets)

    def sort_by_time(self):
        """
            Sorts the packets in the packets list based on their time attribute.
        """
        print(f"\t\tSorting")
        self.packets.sort(key=lambda x: x.time)

    def write_merged_pcap(self, output_file='./Generated/Generated_scenario.pcap'):
        """
            Writes the sorted packets to a merged PCAP file.

            Args:
                output_file (str, optional): The output file path for the merged PCAP file.
                    Defaults to './Generated/Generated.pcap'.
        """

        print("\t[+] Writing packets to Generated.pcap")
        self.sort_by_time()
        wrpcap(output_file, self.packets)
        print("\t\tWritten to ./Generated/Generated.pcap")

    def load_normal_traffic(self):
        """
            Load normal traffic from Normal_traffic.pcap.
        """

        self.normal_traffic_packets = rdpcap(self.normal_traffic_pcap)

    def run_merger(self):
        """
            Runs the entire PCAP merging process by calling the necessary methods.
        """

        self.read_pcap_files()
        # self.load_normal_traffic()        # TODO: Uncomment this line
        self.merge_pcap_files()
        self.write_merged_pcap()

# Example usage
# merger = PcapMerger('/path/to/pcap/directory')
# merger.read_pcap_files()
# merger.merge_pcap_files()
# merger.write_merged_pcap()
