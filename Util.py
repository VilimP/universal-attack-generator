from scapy.utils import wrpcap


def define_attack(attack_type: str):
    """
        Defines the input and output file path for the attack template based on the attack type.

        Args:
            attack_type (str): The type of attack to be generated.

        Returns:
            tuple: A tuple containing the input file path and the output file path.
    """

    # TODO: Makni ove silne if else statemente!
    # TODO: Napravi da ime napada bude jednako imenu datoteke pa je lako time baratati
    # 1: Protocols:  {'Ether', 'IP', 'TCP'}
    if attack_type == "ack_fin_flood":
        input_file = "Attack_templates/DDoS_General/ack-fin.pcap"
    elif attack_type == "ack_psh_fin_flood":
        input_file = "Attack_templates/DDoS_General/ack-psh-fin.pcap"
    elif attack_type == "ack_psh_rst_fin_flood":
        input_file = "Attack_templates/DDoS_General/ack-psh-rst-fin.pcap"
    elif attack_type == "ACK_PSH_RST_shorter_flood":
        input_file = "Attack_templates/DDoS_General/ACK-PSH-RST-shorter.pcap"
    elif attack_type == "ack_psh_rst_syn_fin_flood":
        input_file = "Attack_templates/DDoS_General/ack-psh-rst-syn-fin.pcap"
    elif attack_type == "ack_psh_rst_syn_flood":
        input_file = "Attack_templates/DDoS_General/ack-psh-rst-syn.pcap"
    elif attack_type == "ack_psh_syn_fin_flood":
        input_file = "Attack_templates/DDoS_General/ack-psh-syn-fin.pcap"
    elif attack_type == "ack_psh_syn_flood":
        input_file = "Attack_templates/DDoS_General/ack-psh-syn.pcap"
    elif attack_type == "ack_psh_flood":
        input_file = "Attack_templates/DDoS_General/ack-psh.pcap"
    elif attack_type == "ack_rst_fin_flood":
        input_file = "Attack_templates/DDoS_General/ack-rst-fin.pcap"
    elif attack_type == "ack_rst_syn_fin_flood":
        input_file = "Attack_templates/DDoS_General/ack-rst-syn-fin.pcap"
    elif attack_type == "ACK_RST_SYN_shorter_flood":
        input_file = "Attack_templates/DDoS_General/ACK-RST-SYN-shorter.pcap"
    elif attack_type == "ack_rst_flood":
        input_file = "Attack_templates/DDoS_General/ack-rst.pcap"
    elif attack_type == "ack_syn_fin_flood":
        input_file = "Attack_templates/DDoS_General/ack-syn-fin.pcap"
    elif attack_type == "ack_syn_flood":
        input_file = "Attack_templates/DDoS_General/ack-syn.pcap"
    elif attack_type == "all_tcp_flags_flood":
        input_file = "Attack_templates/DDoS_General/all-flags.pcap"
    elif attack_type == "psh_fin_flood":
        input_file = "Attack_templates/DDoS_General/psh-fin.pcap"
    elif attack_type == "psh_rst_fin_flood":
        input_file = "Attack_templates/DDoS_General/psh-rst-fin.pcap"
    elif attack_type == "psh_rst_syn_fin_flood":
        input_file = "Attack_templates/DDoS_General/psh-rst-syn-fin.pcap"
    elif attack_type == "PSH_SYN_FIN_flood":
        input_file = "Attack_templates/DDoS_General/PSH-SYN-FIN.pcap"
    elif attack_type == "psh_syn_flood":
        input_file = "Attack_templates/DDoS_General/psh-syn.pcap"
    elif attack_type == "PSH_flood":
        input_file = "Attack_templates/DDoS_General/PSH.pcap"
    elif attack_type == "push_reset_flood":
        input_file = "Attack_templates/DDoS_General/push-reset-flood.pcap"
    elif attack_type == "RST_FIN_flood":
        input_file = "Attack_templates/DDoS_General/RST-FIN.pcap"
    elif attack_type == "RST_SYN_FIN_flood":
        input_file = "Attack_templates/DDoS_General/RST-SYN-FIN.pcap"
    elif attack_type == "rst_syn_flood":
        input_file = "Attack_templates/DDoS_General/rst-syn.pcap"
    elif attack_type == "rst_flood":
        input_file = "Attack_templates/DDoS_General/rst.pcap"
    elif attack_type == "empty_connection_flood":
        input_file = "Attack_templates/DDoS_General/empty-connection.pcap"
    elif attack_type == "fin_flood":
        input_file = "Attack_templates/DDoS_General/fin-flood.pcap"
    elif attack_type == "SYN_FIN_flood":
        input_file = "Attack_templates/DDoS_General/SYN-FIN.pcap"
    elif attack_type == "SYN_flood":
        input_file = "Attack_templates/DDoS_General/SYN.pcap"
    elif attack_type == "urg_ack_fin_flood":
        input_file = "Attack_templates/DDoS_General/urg-ack-fin.pcap"
    elif attack_type == "URG_ACK_PSH_FIN_flood":
        input_file = "Attack_templates/DDoS_General/URG-ACK-PSH-FIN.pcap"
    elif attack_type == "urg_ack_psh_rst_fin1_flood":
        input_file = "Attack_templates/DDoS_General/urg-ack-psh-rst-fin1.pcap"
    elif attack_type == "URG_ACK_PSH_RST_flood":
        input_file = "Attack_templates/DDoS_General/URG-ACK-PSH-RST.pcap"
    elif attack_type == "urg_ack_psh_syn_fin_flood":
        input_file = "Attack_templates/DDoS_General/urg-ack-psh-syn-fin.pcap"
    elif attack_type == "URG_ACK_PSH_flood":
        input_file = "Attack_templates/DDoS_General/URG-ACK-PSH.pcap"
    elif attack_type == "urg_ack_rst_fin_flood":
        input_file = "Attack_templates/DDoS_General/urg-ack-rst-fin.pcap"
    elif attack_type == "URG_ACK_RST_SYN_FIN_flood":
        input_file = "Attack_templates/DDoS_General/URG-ACK-RST-SYN-FIN.pcap"
    elif attack_type == "URG_ACK_RST_SYN_shorter_flood":
        input_file = "Attack_templates/DDoS_General/URG-ACK-RST-SYN-shorter.pcap"
    elif attack_type == "URG_ACK_RST_flood":
        input_file = "Attack_templates/DDoS_General/URG-ACK-RST.pcap"
    elif attack_type == "urg_ack_syn_fin1_flood":
        input_file = "Attack_templates/DDoS_General/urg-ack-syn-fin1.pcap"
    elif attack_type == "urg_ack_syn_flood":
        input_file = "Attack_templates/DDoS_General/urg-ack-syn.pcap"
    elif attack_type == "URG_ACK_flood":
        input_file = "Attack_templates/DDoS_General/URG-ACK.pcap"
    elif attack_type == "URG_FIN_flood":
        input_file = "Attack_templates/DDoS_General/URG-FIN.pcap"
    elif attack_type == "URG_PSH_FIN_flood":
        input_file = "Attack_templates/DDoS_General/URG-PSH-FIN.pcap"
    elif attack_type == "urg_psh_rst_fin_flood":
        input_file = "Attack_templates/DDoS_General/urg-psh-rst-fin.pcap"
    elif attack_type == "urg_psh_rst_syn_fin_flood":
        input_file = "Attack_templates/DDoS_General/urg-psh-rst-syn-fin.pcap"
    elif attack_type == "urg_psh_rst1_flood":
        input_file = "Attack_templates/DDoS_General/urg-psh-rst1.pcap"
    elif attack_type == "urg_psh_syn_fin_flood":
        input_file = "Attack_templates/DDoS_General/urg-psh-syn-fin.pcap"
    elif attack_type == "URG_PSH_SYN_flood":
        input_file = "Attack_templates/DDoS_General/URG-PSH-SYN.pcap"
    elif attack_type == "URG_PSH_flood":
        input_file = "Attack_templates/DDoS_General/URG-PSH.pcap"
    elif attack_type == "urg_rst_fin_flood":
        input_file = "Attack_templates/DDoS_General/urg-rst-fin.pcap"
    elif attack_type == "urg_rst_syn_fin1_flood":
        input_file = "Attack_templates/DDoS_General/urg-rst-syn-fin1.pcap"
    elif attack_type == "urg_rst_syn_flood":
        input_file = "Attack_templates/DDoS_General/urg-rst-syn.pcap"
    elif attack_type == "urg_rst_flood":
        input_file = "Attack_templates/DDoS_General/urg-rst.pcap"
    elif attack_type == "urg_syn_fin_flood":
        input_file = "Attack_templates/DDoS_General/urg-syn-fin.pcap"
    elif attack_type == "URG_SYN_flood":
        input_file = "Attack_templates/DDoS_General/URG-SYN.pcap"
    elif attack_type == "urg_flood":
        input_file = "Attack_templates/DDoS_General/urg.pcap"

    # 2: Protocols:  {'Ether', 'Raw', 'IP', 'TCP'}
    elif attack_type == "apache_benchmark_http":
        input_file = "Attack_templates/DDoS_General/ab.pcap"
    elif attack_type == "Cloudscraper_HTTP_DELETE_Flood":
        input_file = "Attack_templates/DDoS_General/Cloudscraper-HTTP-DELETE-Flood.pcap"
    elif attack_type == "Cloudscraper_HTTP_Empty_POST_Flood":
        input_file = "Attack_templates/DDoS_General/Cloudscraper-HTTP-Empty-POST-Flood.pcap"
    elif attack_type == "Cloudscraper_HTTP_GET_Flood_3":
        input_file = "Attack_templates/DDoS_General/Cloudscraper-HTTP-GET-Flood-3.pcap"
    elif attack_type == "Cloudscraper_HTTP_HEAD_Flood":
        input_file = "Attack_templates/DDoS_General/Cloudscraper-HTTP-HEAD-Flood.pcap"
    elif attack_type == "Cloudscraper_HTTP_PUT_Flood":
        input_file = "Attack_templates/DDoS_General/Cloudscraper-HTTP-PUT-Flood.pcap"
    elif attack_type == "head_mhddos":
        input_file = "Attack_templates/DDoS_General/HEAD.pcap"
    elif attack_type == "HTTP_CONNECT_FLOOD":
        input_file = "Attack_templates/DDoS_General/HTTP-CONNECT-FLOOD.pcap"
    elif attack_type == "HTTP_DELETE_FLOOD":
        input_file = "Attack_templates/DDoS_General/HTTP-DELETE-FLOOD.pcap"
    elif attack_type == "http_flood":
        input_file = "Attack_templates/DDoS_General/http-flood.pcap"
    elif attack_type == "http_get_flood_2":
        input_file = "Attack_templates/DDoS_General/http-get-flood-2.pcap"
    elif attack_type == "HTTP_HEAD_FLOOD":
        input_file = "Attack_templates/DDoS_General/HTTP-HEAD-FLOOD.pcap"
    elif attack_type == "HTTP_OPTIONS_FLOOD":
        input_file = "Attack_templates/DDoS_General/HTTP-OPTIONS-FLOOD.pcap"
    elif attack_type == "HTTP_PATCH_FLOOD":
        input_file = "Attack_templates/DDoS_General/HTTP-PATCH-FLOOD.pcap"
    elif attack_type == "http_post_flood_2":
        input_file = "Attack_templates/DDoS_General/http-post-flood-2.pcap"
    elif attack_type == "HTTP_PUT_FLOOD":
        input_file = "Attack_templates/DDoS_General/HTTP-PUT-FLOOD.pcap"
    elif attack_type == "HTTP_TRACE_FLOOD":
        input_file = "Attack_templates/DDoS_General/HTTP-TRACE-FLOOD.pcap"
    elif attack_type == "http_be_20sec":
        input_file = "Attack_templates/DDoS_General/http_be_20sec.pcap"
    elif attack_type == "KB_FLOOD_HTTP_DYNAMIC":
        input_file = "Attack_templates/DDoS_General/KB-FLOOD-HTTP-DYNAMIC.pcap"
    elif attack_type == "SlowLoris":
        input_file = "Attack_templates/DDoS_General/SlowLoris.pcap"
    elif attack_type == "SSL_renegotiation":
        input_file = "Attack_templates/DDoS_General/SSL_renegotiation.pcap"
    elif attack_type == "tcp_rewrite_3":
        input_file = "Attack_templates/DDoS_General/tcp_rewrite-3.pcap"
    elif attack_type == "tcp_rewrite_5":
        input_file = "Attack_templates/DDoS_General/tcp_rewrite-5.pcap"
    elif attack_type == "thc_ssl":
        input_file = "Attack_templates/DDoS_General/thc-ssl.pcap"
    elif attack_type == "tor":
        input_file = "Attack_templates/DDoS_General/tor.pcap"
    elif attack_type == "GoldenEye":
        input_file = "Attack_templates/DDoS_General/GoldenEye.pcap"

    # 3: Protocols:  {'Ether', 'IP', 'Raw', 'TCP', 'Padding'}
    elif attack_type == "CFB":
        input_file = "Attack_templates/DDoS_General/CFB.pcap"
    elif attack_type == "Cloudscraper_HTTP_OPTIONS_Flood":
        input_file = "Attack_templates/DDoS_General/Cloudscraper-HTTP-OPTIONS-Flood.pcap"
    elif attack_type == "cookie_2":
        input_file = "Attack_templates/DDoS_General/cookie-2.pcap"
    elif attack_type == "even_2":
        input_file = "Attack_templates/DDoS_General/even-2.pcap"
    elif attack_type == "get_mhddos":
        input_file = "Attack_templates/DDoS_General/get-mhddos.pcap"
    elif attack_type == "GSB_R":
        input_file = "Attack_templates/DDoS_General/GSB-R.pcap"
    elif attack_type == "hulk":
        input_file = "Attack_templates/DDoS_General/hulk.pcap"
    elif attack_type == "ip_fragmented_garbage":
        input_file = "Attack_templates/DDoS_General/ip_fragmented_garbage.pcap"

    # 4: Protocols:  {'UDP', 'Ether', 'IP', 'Raw', 'TCP', 'Padding'}
    elif attack_type == "Cloudscraper_PATCH_Flood":
        input_file = "Attack_templates/DDoS_General/Cloudscraper-PATCH-Flood.pcap"

    # 5: Protocols:  {'UDP', 'Ether', 'DNS', 'IP'}
    elif attack_type == "dns_sec":
        input_file = "Attack_templates/DDoS_General/dns-sec.pcap"
    elif attack_type == "dns":
        input_file = "Attack_templates/DDoS_General/dns.pcap"

    # 6: Protocols:  {'Ether', 'ICMP', 'IP'}
    elif attack_type == "icmp_ping":
        input_file = "Attack_templates/DDoS_General/icmp-ping.pcap"

    # 7: Protocols:  {'UDP', 'Ether', 'Raw', 'IP'}
    elif attack_type == "SIP_New":
        input_file = "Attack_templates/DDoS_General/SIP-New.pcap"
    elif attack_type == "udp_flood":
        input_file = "Attack_templates/DDoS_General/udp_flood.pcap"
    elif attack_type == "udp_garbage_flood":
        input_file = "Attack_templates/DDoS_General/udp_garbage_flood.pcap"

    # 8: Protocols:  {'UDP', 'Ether', 'IP', 'DNS', 'IPerror', 'ICMP'}
    elif attack_type == "dns_response":
        input_file = "Attack_templates/DDoS_General/dns_response.pcap"

    # 9: Protocols:  {'Ether', 'IP', 'IPerror', 'TCP', 'ICMP', 'Padding'}
    elif attack_type == "ICMP_Destination_unreachable":
        input_file = "Attack_templates/DDoS_General/ICMP-Destination-unreachable.pcap"
    elif attack_type == "ICMP_Time_exceeded":
        input_file = "Attack_templates/DDoS_General/ICMP-Time-exceeded.pcap"

    # 10: Protocols:  {'UDP', 'LLMNRQuery', 'MobileIP', 'RIP', 'Raw', 'VXLAN', 'Radius', 'ISAKMP_payload', 'L2TP', 'HSRPmd5', 'Ether', 'HSRP', 'ZEP2', 'NetflowHeader', 'IP', 'RIPEntry', 'IPerror', 'DNS', 'ISAKMP', 'PPP', 'ICMP', 'MGCP', 'ESP'}
    elif attack_type == "tcp_rewrite_6":
        input_file = "Attack_templates/DDoS_General/tcp_rewrite-6.pcap"

    # 11: Protocols:  {'UDP', 'Ether', 'IP', 'Raw', 'IPerror', 'ICMP'}
    elif attack_type == "UDP_Small_Garbage":
        input_file = "Attack_templates/DDoS_General/UDP_Small_Garbage.pcap"

    # 12: Protocols:  {'Ether', 'Padding', 'IP', 'TCP'}
    elif attack_type == "URG_ACK_PSH_SYN_shorter":
        input_file = "Attack_templates/DDoS_General/URG-ACK-PSH-SYN-shorter.pcap"

    # 13: Protocols:  {'IP', 'Raw', 'CookedLinux', 'TCP', 'Padding'}
    elif attack_type == "brust":
        input_file = "Attack_templates/DDoS_General/brust.pcap"

    # 14: Protocols:  {'UDP', 'Ether', 'IP', 'Raw', 'NTP v??, ??'}
    elif attack_type == "KB_FLOOD_NTP_AMPLIFICATION_REFLECTION_FLOOD":
        input_file = "Attack_templates/DDoS_General/KB-FLOOD-NTP-AMPLIFICATION-REFLECTION-FLOOD.pcap"

    # nmap
    elif attack_type == "OS_detection_scan":
        input_file = "Attack_templates/nmap/OS_detection_scan.pcapng"
    elif attack_type == "scan_network_for_hosts":
        input_file = "Attack_templates/nmap/scan_network_for_hosts.pcapng"
    elif attack_type == "Script_address-info_scan":
        input_file = "Attack_templates/nmap/Script_address-info_scan.pcapng"
    elif attack_type == "Script_ajp-auth_scan":
        input_file = "Attack_templates/nmap/Script_ajp-auth_scan.pcapng"
    elif attack_type == "Script_auth-owners":
        input_file = "Attack_templates/nmap/Script_auth-owners.pcapng"
    elif attack_type == "Script_broadcast-jenkins-discover":
        input_file = "Attack_templates/nmap/Script_broadcast-jenkins-discover.pcapng"
    elif attack_type == "Script_ssh-auth-methods":
        input_file = "Attack_templates/nmap/Script_ssh-auth-methods.pcapng"
    elif attack_type == "Script_ssh-brute":
        input_file = "Attack_templates/nmap/Script_ssh-brute.pcapng"
    elif attack_type == "Script_ssh-hostkey":
        input_file = "Attack_templates/nmap/Script_ssh-hostkey.pcapng"
    elif attack_type == "Script_ssh-run":
        input_file = "Attack_templates/nmap/Script_ssh-run.pcapng"
    elif attack_type == "SCTP_COOKIE-ECHO_scan":
        input_file = "Attack_templates/nmap/SCTP_COOKIE-ECHO_scan.pcapng"
    elif attack_type == "SCTP_INIT_scan":
        input_file = "Attack_templates/nmap/SCTP_INIT_scan.pcapng"
    elif attack_type == "TCP_ACK_scan":
        input_file = "Attack_templates/nmap/TCP_ACK_scan.pcapng"
    elif attack_type == "TCP_connect_scan":
        input_file = "Attack_templates/nmap/TCP_connect_scan.pcapng"
    elif attack_type == "TCP_Maimon_scan":
        input_file = "Attack_templates/nmap/TCP_Maimon_scan.pcapng"
    elif attack_type == "TCP_null_scan":
        input_file = "Attack_templates/nmap/TCP_null_scan.pcapng"
    elif attack_type == "tcp_syn_scan":
        input_file = "Attack_templates/nmap/tcp_syn_scan.pcap"
    elif attack_type == "TCP_SYN_scan":
        input_file = "Attack_templates/nmap/TCP_SYN_scan.pcapng"
    elif attack_type == "TCP_window_scan":
        input_file = "Attack_templates/nmap/TCP_window_scan.pcapng"
    elif attack_type == "TCP_xmas_scan":
        input_file = "Attack_templates/nmap/TCP_xmas_scan.pcapng"
    elif attack_type == "UDP_scan":
        input_file = "Attack_templates/nmap/UDP_scan.pcapng"
    elif attack_type == "version_detection_scan":
        input_file = "Attack_templates/nmap/version_detection_scan.pcapng"

    # tools
    elif attack_type == "arping_c_1_ip":
        input_file = "Attack_templates/tools/arping_c_1_ip.pcapng"
    elif attack_type == "dmitry_-winsepfb_ip":
        input_file = "Attack_templates/tools/dmitry_-winsepfb_ip.pcapng"
    elif attack_type == "enum4linux_a_ip":
        input_file = "Attack_templates/tools/enum4linux_a_ip.pcapng"
    elif attack_type == "fping_c_1_ip":
        input_file = "Attack_templates/tools/fping_c_1_ip.pcapng"
    elif attack_type == "fping_c_10_ttl_10__R_ip":
        input_file = "Attack_templates/tools/fping_c_10_ttl_10__R_ip.pcapng"
    elif attack_type == "fping_c_10_ttl_10_ip":
        input_file = "Attack_templates/tools/fping_c_10_ttl_10_ip.pcapng"
    elif attack_type == "fping_c_50_ip":
        input_file = "Attack_templates/tools/fping_c_50_ip.pcapng"
    elif attack_type == "hping3_icmp_c_3_ip":
        input_file = "Attack_templates/tools/hping3_icmp_c_3_ip.pcapng"
    elif attack_type == "hping3_S_p_80_c_3_ip":
        input_file = "Attack_templates/tools/hping3_S_p_80_c_3_ip.pcapng"
    elif attack_type == "hping3_scan_1-1000_S_ip":
        input_file = "Attack_templates/tools/hping3_scan_1-1000_S_ip.pcapng"
    elif attack_type == "ike-scan_M_ip":
        input_file = "Attack_templates/tools/ike-scan_M_ip.pcapng"
    elif attack_type == "legion_easy_host-disc,staged_nmap_insane":
        input_file = "Attack_templates/tools/legion_easy_host-disc,staged_nmap_insane.pcapng"
    elif attack_type == "legion_easy_host-disc,staged_nmap_sneaky":
        input_file = "Attack_templates/tools/legion_easy_host-disc,staged_nmap_sneaky.pcapng"
    elif attack_type == "legion_hard_insane_obfuscated_timestamp":
        input_file = "Attack_templates/tools/legion_hard_insane_obfuscated_timestamp.pcapng"
    elif attack_type == "legion_hard_paranoid_obfuscated_TCP-SYN":
        input_file = "Attack_templates/tools/legion_hard_paranoid_obfuscated_TCP-SYN.pcapng"
    elif attack_type == "netdiscover_r_ip_scan":
        input_file = "Attack_templates/tools/netdiscover_r_ip_scan.pcapng"

    elif attack_type == "Port Scan":
        input_file = "Attack_templates/Scenario_pcaps/Port Scan.pcapng"
    elif attack_type == "Service Scan":
        input_file = "Attack_templates/Scenario_pcaps/Service Scan.pcapng"
    elif attack_type == "Host Scan":
        input_file = "Attack_templates/Scenario_pcaps/Host Scan.pcap"
    elif attack_type == "Apache Struts2 code execution":
        input_file = "Attack_templates/Scenario_pcaps/Apache Struts2 code execution.pcap"
    elif attack_type == "Backdoor Windows XP CMDEXE Reverse Shell":
        input_file = "Attack_templates/Scenario_pcaps/Backdoor Windows XP CMDEXE Reverse Shell.pcap"

    else:
        print("Nepoznati napad!")
        exit()
    # output_file = "./Generated/output.pcap"
    output_file = "./Generated/" + str(input_file.split("/")[2].split(".")[0]) + "_gen.pcap"

    return input_file, output_file


def generate_pcap(output_file, packets):
    """
        Generates a PCAP file containing the provided packets.

        Args:
            output_file (str): The path to the output PCAP file.
            packets (list): A list of packets to be written to the PCAP file.
    """

    sorted_packets = sorted(packets, key=lambda x: x.time)
    wrpcap(output_file, sorted_packets)


def calc_scale_factor(packets, duration):
    """
        Calculates the scale factor for packet timing based on the desired duration.

        Args:
            packets (list): A list of packets.
            duration (float): The desired duration for the scaled packets.

        Returns:
            int: The scale factor to be applied to the packet timing.
    """

    start_time = packets[0].time
    end_time = packets[-1].time

    scale_factor = int(duration / (end_time - start_time))

    print("\t\t\t\t[+] Scaling")
    print("\t\t\t\t\tOriginal duration: ", (end_time - start_time))
    print("\t\t\t\t\tWanted duration: ", duration)
    print("\t\t\t\t\tScale factor: ", scale_factor)

    return scale_factor
