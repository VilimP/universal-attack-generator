import yaml
from Attack_generator import AttackGenerator
from Static_generator import StaticGenerator
from TopologyFormatter import find_node, find_path
from Victim_generator import VictimGenerator
from Util import *


def execute_tactic(tactic, parameters, topology):
    """
        This function executes a specific tactic within an attack scenario.
        It simulates the execution of the tactic, defines input and output file paths,
        creates instances of AttackGenerator and VictimGenerator classes,
        calculates a scale factor, modifies and scales the attack and victim packets,
        merges the scaled packets, and generates a PCAP file containing the merged packets.

        If attack tactic from scenario is defined as static (field "static: True/False in scenario file") it will
        instantiate StaticGenerator instead of AttackGenerator or VictimGenerator. StaticGenerator can't scale attack or
        change its intensity, only modify parameters.
    """

    print(f"\t\tExecuting tactic: {tactic['name']}")

    # Definiranje ulazne i izlazne datoteke
    input_file, output_file = define_attack(tactic['name'])

    # TODO: nađi bolji način da odrediš da li je potrebno generirati pcap datoteke na svakom ruteru na putu do žrtve
    # walk = True
    #
    # # find path from attacker to victim
    # attacker_ip = parameters["Attacker IP"]
    # victim_ip = parameters["Victim IP"]
    # start_search_node = topology["nodes"][0]
    #
    # start_node = find_node(topology, "10.0.51.26")
    # end_node = find_node(topology, "10.0.0.9")
    #
    # # TODO: provjeri što ne radi u funkciji find_path()!
    # path = find_path(topology, start_node, end_node)

    # while True:

    if tactic["static"]:
        # Stvaranje objekata za generiranje napada i prometa žrtve
        static_gen = StaticGenerator(input_file, tactic['name'])

        # Modifikacija i skaliranje prometa napadača
        print("\t\t\t\t[+] Modification")
        for key, value in parameters.items():
            print(f"\t\t\t\t\t{key}: {value}")
        static_gen.modify(parameters)

        if 'start_attack' in parameters:
            static_gen.shift_packet_times(parameters['start_attack'])

        # Spajanje skaliranih prometa napadača i žrtve
        print("\t\t\t\t[+] Merging")
        generated_packets = list()
        generated_packets.extend(static_gen.victim_packets)
        generated_packets.extend(static_gen.attacker_packets)

    else:
        # Stvaranje objekata za generiranje napada i prometa žrtve
        attack_gen = AttackGenerator(input_file, tactic['name'])
        victim_gen = VictimGenerator(input_file, tactic['name'])

        # Izračun faktora skaliranja
        scale_factor = calc_scale_factor(attack_gen.packets, parameters["duration"] if "duration" in parameters else 1)

        # Modifikacija i skaliranje prometa napadača
        print("\t\t\t\t[+] Modification")
        for key, value in parameters.items():
            print(f"\t\t\t\t\t{key}: {value}")
        attack_gen.modify(parameters)
        attack_gen.scale_attack(scale_factor)

        # Modifikacija i skaliranje prometa žrtve
        victim_gen.modify(parameters)
        victim_gen.scale_attack(scale_factor)

        # Promjena vremena početka napada
        if 'start_attack' in parameters:
            attack_gen.shift_packet_times(parameters['start_attack'])
            victim_gen.shift_packet_times(parameters['start_attack'])

        # Spajanje skaliranih prometa napadača i žrtve
        print("\t\t\t\t[+] Merging")
        generated_packets = list()
        generated_packets.extend(victim_gen.victim_packets)
        generated_packets.extend(attack_gen.attacker_packets)

    # Generiranje PCAP datoteke
    print("\t\t\t\t[+] Writing to pcap")
    print("\t\t\t\t\tNumber of packets:", len(generated_packets))
    generate_pcap(output_file, generated_packets)

    print(f"\t\t\t=> Generated {tactic['name']} tactic in PCAP file: {output_file}\n")

    # if not walk:
    #     break


def execute_phase(phase_name, phase_details, topology):
    """
        This function executes a phase within an attack scenario.
        It prints the phase name and any general information provided in the phase details,
        and iterates over the tactics defined in the phase details, calling execute_tactic for each tactic.
    """

    print(f"\tExecuting phase: {phase_name.upper()}")
    general_info = phase_details.pop("general_info", {})
    if general_info:
        print("\t\tGeneral Information:")
        for key, value in general_info.items():
            print(f"\t\t\t{key}: {value}")

    parameters = general_info
    del parameters["description"]

    tactics = phase_details.pop("tactics", [])
    for tactic in tactics:
        execute_tactic(tactic, parameters, topology)


def execute_attack_scenario(attack_scenario, topology):
    """
        This function executes an entire attack scenario.
        It iterates over the phases defined in the attack scenario and calls execute_phase for each phase.
    """

    # print("\tAttack Scenario Execution Started")
    for phase_name, phase_details in attack_scenario.items():
        execute_phase(phase_name, phase_details, topology)
    # print("\nAttack Scenario Execution Completed")


def run_parser(scenario_file, topology):
    """
        This function is the entry point of the script.
        It loads the attack scenario from the provided YAML file,
        calls execute_attack_scenario to execute the loaded attack scenario,
        and returns the loaded attack scenario.
    """

    # Load the attack scenario from YAML file
    with open(scenario_file, "r") as file:
        print("\tReading scenario file")
        attack_scenario = yaml.safe_load(file)

    # Execute the attack scenario
    print("\n[3] Attack scenario generation")
    execute_attack_scenario(attack_scenario["attack_model"], topology)

    return attack_scenario

# run("Scenario/Scenario-template.yml")
