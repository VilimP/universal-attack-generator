import argparse
import time

import TopologyFormatter
import ScenarioParser
from PcapMerger import PcapMerger


class UniversalAttackGenerator:
    def __init__(self, topology_file, scenario_file):
        self.topology_file = topology_file
        self.scenario_file = scenario_file

    def run(self):
        # Obrada topologije
        print("[1] Obrada topologije")
        topology_file = self.topology_file   # napiši samo ime datoteke, podrazumijevana putanja ./Topologija/{topology_file}
        topology = TopologyFormatter.run_formatter(topology_file)
        print()

        # Učitavanje scenarija napada
        print("[2] Obrada scenarija")
        scenario_file = self.scenario_file
        scenario = ScenarioParser.run_parser(scenario_file, topology)

        # TODO: After reading Scenario, for EACH step in scenario generate packets!!!
        # TODO: Način 1. (call generator for each step of attack)
        # TODO: Način 2. (handle scenario inside generator)

        print("[4] Spajanje .pcap datoteka")
        merger = PcapMerger("./Generated", "./Generated/Normal_traffic.pcap")
        merger.run_merger()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Generate network attack traffic based on given topology and scenario files.")
    parser.add_argument("topology_file", type=str, help="The path to the topology file.")
    parser.add_argument("scenario_file", type=str, help="The path to the scenario file.")
    args = parser.parse_args()

    time_start = time.time()

    generator = UniversalAttackGenerator(args.topology_file, args.scenario_file)
    generator.run()

    time_end = time.time()

    print(f"\n=> Attack scenario generated in {time_end - time_start}s")


