from scapy.all import *
from scapy.layers.http import HTTP
from scapy.layers.inet import IP, TCP, UDP, ICMP
from scapy.layers.l2 import Ether
from scapy.packet import Raw
from scapy.layers.dns import DNS


class StaticGenerator:
    """
        A class to generate and modify static network attack packets.

        The `StaticGenerator` class reads packets from a pcap file, identifies attacker
        and victim information, and provides methods to modify packets based on given parameters.

        The `StaticGenerator` class SHOULD NOT be scaled, changed intensity or do anything that increases or decreases
        number of packets in attack becouse it generates exploit packets or similar attacks that are not scalable.

        Attributes:
            packets (list): A list of packets read from the input pcap file.
            attacker (dict): A dictionary containing information about the attacker and victim.
            attacker_packets (list): A list of packets originating from the attacker.
            victim_packets (list): A list of packets originating from the victim.
            attack (str): The type of attack to be simulated or analyzed.

        Methods:
            define_attacker(): Extract and return attacker and victim information from the first packet.
            modify(parameters): Modify attacker packets based on provided parameters.
            razdvoji_pakete(): Separate packets into attacker and victim categories based on their IP addresses.
    """

    def __init__(self, input_file, attack):
        self.packets = rdpcap(input_file)
        self.attacker = self.define_attacker()
        self.attacker_packets, self.victim_packets = self.razdvoji_pakete()
        self.attack = attack

    def modify(self, parameters):
        """
            Modify attacker packets based on provided parameters.

            This function iterates over the list of attacker packets (`self.attacker_packets`) and modifies
            various fields based on the provided `parameters` dictionary. The fields that can be modified
            include MAC addresses, IP addresses, TTL, TCP flags, sequence and acknowledgment numbers,
            window size, and specific HTTP, UDP, DNS, and ICMP fields.

            Returns:
                list: The modified list of attacker packets.
        """

        for atk_packet in self.attacker_packets:
            if Ether in atk_packet:
                if "MAC Attacker" in parameters:
                    if atk_packet[Ether].src == self.attacker["Attacker MAC"]:
                        atk_packet[Ether].src = parameters["Attacker MAC"]
                    if atk_packet[Ether].dst == self.attacker["Attacker MAC"]:
                        atk_packet[Ether].dst = parameters["Attacker MAC"]
                if "MAC Victim" in parameters:
                    if atk_packet[Ether].src == self.attacker["Victim MAC"]:
                        atk_packet[Ether].src = parameters["Victim MAC"]
                    if atk_packet[Ether].dst == self.attacker["Victim MAC"]:
                        atk_packet[Ether].dst = parameters["Victim MAC"]

            if IP in atk_packet:
                if "IP Attacker" in parameters:
                    if atk_packet[IP].src == self.attacker["Atacker IP"]:
                        atk_packet[IP].src = parameters["Attacker IP"]
                    if atk_packet[IP].dst == self.attacker["Atacker IP"]:
                        atk_packet[IP].dst = parameters["Attacker IP"]
                if "IP Victim" in parameters:
                    if atk_packet[IP].src == self.attacker["Victim IP"]:
                        atk_packet[IP].src = parameters["Victim IP"]
                    if atk_packet[IP].dst == self.attacker["Victim IP"]:
                        atk_packet[IP].dst = parameters["Victim IP"]
                # if "Flags" in parameters:
                #     packet_orig[IP].flags = parameters["Flags"]
                if "TTL" in parameters:
                    atk_packet[IP].ttl = parameters["TTL"]
                # if "Protocol" in parameters:
                #     packet_orig[IP].proto = parameters["Protocol"]

                if TCP in atk_packet:
                    # TODO: Odredi portove napadača i žrtve
                    # if "Port Victim" in parameters:
                    #     if atk_packet[TCP].sport == self.attacker["Victim port"]:
                    #         atk_packet[TCP].sport = parameters["Victim port"]
                    #     else:
                    #         atk_packet[TCP].sport = parameters["Attacker port"]
                    #     if atk_packet[TCP].dport == self.attacker["Victim port"]:
                    #         atk_packet[TCP].dport = parameters["Victim port"]
                    #     else:
                    #         atk_packet[TCP].dport = parameters["Attacker port"]
                    # if "Port Attacker" in parameters:
                    #     if atk_packet[TCP].sport == self.attacker["Attacker port"]:
                    #         atk_packet[TCP].sport = parameters["Attacker port"]
                    #     if atk_packet[TCP].dport == self.attacker["Attacker port"]:
                    #         atk_packet[TCP].dport = parameters["Attacker port"]
                    if "Flags" in parameters:
                        atk_packet[TCP].flags = parameters["Flags"]
                    if "Seq Number" in parameters:
                        atk_packet[TCP].seq = parameters["Seq Number"]
                    if "Ack Number" in parameters:
                        atk_packet[TCP].ack = parameters["Ack Number"]
                    if "Window Size" in parameters:
                        atk_packet[TCP].window = parameters["Window Size"]

                    if "HTTP" in parameters:
                        if atk_packet.haslayer(Raw):
                            if b"HTTP" in atk_packet[Raw].load or b'<!DOCTYPE HTML>' in atk_packet[Raw].load:
                                # ako je paket odgovor, onda se sadržaj (Html) dohvača ovako:
                                load = atk_packet.load
                                if "Raw load" in parameters:
                                    atk_packet[Raw].load = parameters["Raw load"]
                                    print("Previous load :", load, end="\n\n")
                                    print("New load      :", atk_packet[Raw].load, end="\n\n")
                        if atk_packet.haslayer(HTTP):
                            if atk_packet[TCP].sport == "80":
                                date = atk_packet[HTTP].Date
                                server = atk_packet[HTTP].Server
                                last_modified = atk_packet[HTTP].Last_Modified
                                accept_ranges = atk_packet[HTTP].Accept_Ranges
                                content_encoding = atk_packet[HTTP].Content_Encoding
                                keep_alive = atk_packet[HTTP].Keep_Alive
                                connection = atk_packet[HTTP].Connection
                                content_type = atk_packet[HTTP].Content_Type
                                original = atk_packet[HTTP].original

                                atk_packet = Ether
                            if atk_packet[TCP].dport == "80":
                                host = atk_packet[HTTP].Host
                                http_version = atk_packet[HTTP].Http_Version
                                keep_alive = atk_packet[HTTP].Keep_Alive
                                method = atk_packet[HTTP].Method
                                path = atk_packet[HTTP].Path
                                user_agent = atk_packet[HTTP].User_Agent
                                fields = atk_packet[HTTP].payload.fields
                                original = atk_packet[HTTP].original  # -> cijeli zahtjev

                                if "Host" in parameters:
                                    atk_packet[HTTP].Host = parameters["Host"]
                                if "Http Version" in parameters:
                                    atk_packet[HTTP].Http_Version = parameters["Http Version"]
                                if "Keep Alive" in parameters:
                                    atk_packet[HTTP].Keep_Alive = parameters["Keep Alive"]
                                if "Method" in parameters:
                                    atk_packet[HTTP].Method = parameters["Method"]
                                if "Path" in parameters:
                                    atk_packet[HTTP].Path = parameters["Path"]
                                if "User Agent" in parameters:
                                    atk_packet[HTTP].User_Agent = parameters["User Agent"]
                                if "fields" in parameters:
                                    atk_packet[HTTP].payload.fields = parameters["fields"]
                                if "original" in parameters:
                                    atk_packet[HTTP].original = parameters["original"]

                if UDP in atk_packet:
                    if "Source Port" in parameters:
                        atk_packet[UDP].sport = parameters["Source Port"]
                    if "Destination Port" in parameters:
                        atk_packet[UDP].dport = parameters["Destination Port"]
                    if "Window Size" in parameters:
                        atk_packet[TCP].window = parameters["Window Size"]

                    if DNS in atk_packet and atk_packet.haslayer(DNS):
                        if "Queried Domain" in parameters:
                            atk_packet[DNS].qd.qname = parameters["Queried Domain"]  # Extracting the queried domain

                if ICMP in atk_packet and atk_packet.haslayer(ICMP):
                    if "ICMP Type" in parameters:
                        atk_packet[ICMP].type = parameters["ICMP Type"]


        return self.attacker_packets

    def define_attacker(self):
        """
            Extract and return attacker and victim information from the first packet in the list.

            This function processes the first packet in the `self.packets` list to extract details
            about the attacker and the victim. The extracted details include MAC addresses,
            IP addresses, and ports.
        """

        attacker_packet = self.packets[0]

        attacker = {
            "Attacker MAC": attacker_packet[Ether].src if Ether in attacker_packet else None,
            "Victim MAC": attacker_packet[Ether].dst if Ether in attacker_packet else None,
            "Attacker IP": attacker_packet[IP].src,
            "Victim IP": attacker_packet[IP].dst,
            "Attacker port": str(attacker_packet[TCP].sport) if TCP in attacker_packet else str(attacker_packet[UDP].sport) if UDP in attacker_packet else str(attacker_packet[ICMP].sport) if ICMP in attacker_packet else None,
            "Victim port": str(attacker_packet[TCP].dport) if TCP in attacker_packet else str(attacker_packet[UDP].dport) if UDP in attacker_packet else str(attacker_packet[ICMP].dport) if ICMP in attacker_packet else None,
        }

        return attacker

    def razdvoji_pakete(self):
        """
            Separate packets into attacker and victim categories based on their IP addresses.

            This function iterates over the list of packets (`self.packets`) and separates them
            into two lists: one for attacker packets and one for victim packets. The separation
            is based on the source IP address of each packet, comparing it to the stored attacker
            and victim IP addresses.

            Returns:
                tuple: A tuple containing two lists:
                    - paketi_napadac: List of packets where the source IP matches the attacker's IP.
                    - paketi_zrtva: List of packets where the source IP matches the victim's IP.
        """

        paketi_napadac = []
        paketi_zrtva = []

        for paket in self.packets:
            if IP in paket:
                if paket[IP].src == self.attacker["Attacker IP"]:
                    paketi_napadac.append(paket)
                elif paket[IP].src == self.attacker["Victim IP"]:
                    paketi_zrtva.append(paket)

        return paketi_napadac, paketi_zrtva

    def shift_packet_times(self, start_attack):
        """
        Učitava pcap datoteku, pomiče vremena paketa za vrijednost start_attack
        i sprema modificiranu pcap datoteku na disk.

        Argumenti:
        packets (list): Lista mrežnih paketa
        start_attack (int): Vrijeme u sekundama za pomicanje vremena paketa
        """

        time_first_packet = self.attacker_packets[0].time
        start_attack = EDecimal(start_attack)

        # Pomakni vremena napadackih paketa
        modified_packets = list()
        for packet in self.attacker_packets:
            # Dohvati vrijeme paketa kao datetime objekt
            packet_time = packet.time

            # Pomakni vrijeme za start_attack sekundi
            shifted_time = packet_time + (start_attack - time_first_packet)

            # Ažuriraj vrijeme paketa
            packet.time = shifted_time

            modified_packets.append(packet)

        time_first_packet = self.victim_packets[0].time

        # Pomakni vremena zrtvinih paketa
        for packet in self.victim_packets:
            # Dohvati vrijeme paketa kao datetime objekt
            packet_time = packet.time

            # Pomakni vrijeme za start_attack sekundi
            shifted_time = packet_time + (start_attack - time_first_packet)

            # Ažuriraj vrijeme paketa
            packet.time = shifted_time

            modified_packets.append(packet)

        self.attacker_packets = modified_packets
