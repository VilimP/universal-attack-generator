from scapy.all import *
from scapy.layers.http import HTTP
from scapy.layers.inet import IP, TCP, UDP, ICMP
from scapy.layers.l2 import Ether
from scapy.packet import Raw
from scapy.layers.dns import DNS
import copy


class VictimGenerator:
    """
        A class to generate and modify network attack packets.

        The `AttackGenerator` class reads packets from a pcap file, identifies attacker
        and victim information, and provides methods to modify packets based on given parameters.

        Attributes:
            packets (list): A list of packets read from the input pcap file.
            attacker (dict): A dictionary containing information about the attacker and victim.
            attacker_packets (list): A list of packets originating from the attacker.
            victim_packets (list): A list of packets originating from the victim.
            attack (str): The type of attack to be simulated or analyzed.

        Methods:
            define_attacker(): Extract and return attacker and victim information from the first packet.
            modify(parameters): Modify victim packets based on provided parameters.
            generiraj_portove(scale_factor): Generate additional ports based on a scaling factor.
            vremena_izmedu_paketa(victim_packets, scale_factor): Calculate time differences between victim packets.
            gen_additional_packets(vct_packets, scale_factor): Generate additional victim packets for scaling.
            modify_ports(additional_packets, additional_ports): Modify destination ports of additional packets.
            modify_time(additional_packets, vremena_razlika, current_time): Modify timestamps of additional packets.
            scale_attack(scale_factor): Scale the attack by generating and modifying additional packets.
            razdvoji_pakete(): Separate packets into attacker and victim categories based on their IP addresses.
    """

    def __init__(self, input_file, attack):
        self.packets = rdpcap(input_file)
        self.attacker = self.define_attacker()
        self.attacker_packets, self.victim_packets = self.razdvoji_pakete()
        self.attack = attack

    def modify(self, parameters):
        """
            Modify victim packets based on provided parameters.

            Args:
                parameters (dict): Dictionary containing the parameters for modification.
        """

        for vct_packet in self.victim_packets:
            if Ether in vct_packet:
                if "MAC Attacker" in parameters:
                    if vct_packet[Ether].src == self.attacker["Attacker MAC"]:
                        vct_packet[Ether].src = parameters["Attacker MAC"]
                    if vct_packet[Ether].dst == self.attacker["Attacker MAC"]:
                        vct_packet[Ether].dst = parameters["Attacker MAC"]
                if "MAC Victim" in parameters:
                    if vct_packet[Ether].src == self.attacker["Victim MAC"]:
                        vct_packet[Ether].src = parameters["Victim MAC"]
                    if vct_packet[Ether].dst == self.attacker["Victim MAC"]:
                        vct_packet[Ether].dst = parameters["Victim MAC"]

            if IP in vct_packet:
                if "IP Attacker" in parameters:
                    if vct_packet[IP].src == self.attacker["Attacker IP"]:
                        vct_packet[IP].src = parameters["Attacker IP"]
                    if vct_packet[IP].dst == self.attacker["Attacker IP"]:
                        vct_packet[IP].dst = parameters["Attacker IP"]
                if "IP Victim" in parameters:
                    if vct_packet[IP].src == self.attacker["Victim IP"]:
                        vct_packet[IP].src = parameters["Victim IP"]
                    if vct_packet[IP].dst == self.attacker["Victim IP"]:
                        vct_packet[IP].dst = parameters["Victim IP"]
                # if "Flags" in parameters:
                #     packet_orig[IP].flags = parameters["Flags"]
                if "TTL" in parameters:
                    vct_packet[IP].ttl = parameters["TTL"]
                # if "Protocol" in parameters:
                #     packet_orig[IP].proto = parameters["Protocol"]

                if TCP in vct_packet:
                    # TODO: Odredi portove napadača i žrtve
                    # if "Port Victim" in parameters:
                    #     if vct_packet[TCP].sport == self.attacker["Victim port"]:
                    #         vct_packet[TCP].sport = parameters["Victim port"]
                    #     else:
                    #         vct_packet[TCP].sport = parameters["Attacker port"]
                    #     if vct_packet[TCP].dport == self.attacker["Victim port"]:
                    #         vct_packet[TCP].dport = parameters["Victim port"]
                    #     else:
                    #         vct_packet[TCP].dport = parameters["Attacker port"]
                    # if "Port Attacker" in parameters:
                    #     if vct_packet[TCP].sport == self.attacker["Attacker port"]:
                    #         vct_packet[TCP].sport = parameters["Attacker port"]
                    #     if vct_packet[TCP].dport == self.attacker["Attacker port"]:
                    #         vct_packet[TCP].dport = parameters["Attacker port"]
                    if "Flags" in parameters:
                        vct_packet[TCP].flags = parameters["Flags"]
                    if "Seq Number" in parameters:
                        vct_packet[TCP].seq = parameters["Seq Number"]
                    if "Ack Number" in parameters:
                        vct_packet[TCP].ack = parameters["Ack Number"]
                    if "Window Size" in parameters:
                        vct_packet[TCP].window = parameters["Window Size"]

                    if "HTTP" in parameters:
                        if vct_packet.haslayer(Raw):
                            if b"HTTP" in vct_packet[Raw].load or b'<!DOCTYPE HTML>' in vct_packet[Raw].load:
                                # ako je paket odgovor, onda se sadržaj (Html) dohvača ovako:
                                load = vct_packet.load
                                if "Raw load" in parameters:
                                    vct_packet[Raw].load = parameters["Raw load"]
                                    print("Previous load :", load, end="\n\n")
                                    print("New load      :", vct_packet[Raw].load, end="\n\n")
                        if vct_packet.haslayer(HTTP):
                            if vct_packet[TCP].sport == "80":
                                date = vct_packet[HTTP].Date
                                server = vct_packet[HTTP].Server
                                last_modified = vct_packet[HTTP].Last_Modified
                                accept_ranges = vct_packet[HTTP].Accept_Ranges
                                content_encoding = vct_packet[HTTP].Content_Encoding
                                keep_alive = vct_packet[HTTP].Keep_Alive
                                connection = vct_packet[HTTP].Connection
                                content_type = vct_packet[HTTP].Content_Type
                                original = vct_packet[HTTP].original

                                vct_packet = Ether
                            if vct_packet[TCP].dport == "80":
                                host = vct_packet[HTTP].Host
                                http_version = vct_packet[HTTP].Http_Version
                                keep_alive = vct_packet[HTTP].Keep_Alive
                                method = vct_packet[HTTP].Method
                                path = vct_packet[HTTP].Path
                                user_agent = vct_packet[HTTP].User_Agent
                                fields = vct_packet[HTTP].payload.fields
                                original = vct_packet[HTTP].original    #-> cijeli zahtjev

                                if "Host" in parameters:
                                    vct_packet[HTTP].Host = parameters["Host"]
                                if "Http Version" in parameters:
                                    vct_packet[HTTP].Http_Version = parameters["Http Version"]
                                if "Keep Alive" in parameters:
                                    vct_packet[HTTP].Keep_Alive = parameters["Keep Alive"]
                                if "Method" in parameters:
                                    vct_packet[HTTP].Method = parameters["Method"]
                                if "Path" in parameters:
                                    vct_packet[HTTP].Path = parameters["Path"]
                                if "User Agent" in parameters:
                                    vct_packet[HTTP].User_Agent = parameters["User Agent"]
                                if "fields" in parameters:
                                    vct_packet[HTTP].payload.fields = parameters["fields"]
                                if "original" in parameters:
                                    vct_packet[HTTP].original = parameters["original"]

                if UDP in vct_packet:
                    if "Source Port" in parameters:
                        vct_packet[UDP].sport = parameters["Source Port"]
                    if "Destination Port" in parameters:
                        vct_packet[UDP].dport = parameters["Destination Port"]
                    if "Window Size" in parameters:
                        vct_packet[TCP].window = parameters["Window Size"]

                    if DNS in vct_packet and vct_packet.haslayer(DNS):
                        if "Queried Domain" in parameters:
                            vct_packet[DNS].qd.qname = parameters["Queried Domain"]  # Extracting the queried domain

                if ICMP in vct_packet and vct_packet.haslayer(ICMP):
                    if "ICMP Type" in parameters:
                        vct_packet[ICMP].type = parameters["ICMP Type"]

    def generiraj_portove(self, scale_factor):
        """
            Generate a set of additional ports needed for scaling.

            Args:
                scale_factor (int): The factor by which to scale the number of ports.

            Returns:
                set: A set of additional ports.
        """

        # Lista portova koji se koriste u prometu
        postojeci_portovi = set()

        # Pronalazak svih korištenih portova u prometu
        for paket in self.packets:
            if TCP in paket:
                postojeci_portovi.add(paket[TCP].sport)
                postojeci_portovi.add(paket[TCP].dport)
            elif UDP in paket:
                postojeci_portovi.add(paket[UDP].sport)
                postojeci_portovi.add(paket[UDP].dport)
        postojeci_portovi.remove(int(self.attacker["Victim port"]))
        broj_zeljenih_portova = scale_factor * len(postojeci_portovi)

        # Generiranje preostalih portova
        preostali_portovi = set(range(1024, 65536)) - postojeci_portovi

        potrebni_portovi = set()
        for port in preostali_portovi:
            potrebni_portovi.add(port)
            if len(potrebni_portovi) >= broj_zeljenih_portova - len(postojeci_portovi):
                break
        return potrebni_portovi

    def vremena_izmedu_paketa(self, victim_packets, scale_factor):
        """
            Calculate time differences between victim packets.

            Args:
                victim_packets (list): A list of victim packets.
                scale_factor (int): The factor by which to scale the time differences.

            Returns:
                list: A list of scaled time differences between packets.
        """

        vremena = []

        for i in range(1, len(victim_packets)):
            trenutni_paket = victim_packets[i]
            prethodni_paket = victim_packets[i - 1]

            # Dobivanje vremena dolaska za trenutni i prethodni paket
            trenutno_vrijeme = trenutni_paket.time
            prethodno_vrijeme = prethodni_paket.time

            # Izračunavanje razlike u vremenima
            razlika_vremena = trenutno_vrijeme - prethodno_vrijeme

            # Dodavanje razlike u vremenu u listu vremena
            vremena.append(razlika_vremena)

        vremena.append(vremena[-1])

        vremena_mul = copy.deepcopy(vremena)
        for i in range(scale_factor - 2):
            vremena_mul.extend(vremena)

        return vremena_mul

    def gen_additional_packets(self, vct_packets, scale_factor):
        """
            Generate additional packets for scaling.

            Args:
                vct_packets (list): A list of victim packets.
                scale_factor (int): The factor by which to scale the number of packets.

            Returns:
                list: A list of additional packets.
        """

        additional_packets = list()
        for i in range(scale_factor - 1):
            additional_packets.extend(copy.deepcopy(vct_packets))

        return additional_packets

    def modify_ports(self, additional_packets, additional_ports):
        """
            Modify ports of additional packets.

            Args:
                additional_packets (list): A list of additional packets.
                additional_ports (set): A set of additional ports.

            Returns:
                list: The modified list of additional packets.
        """

        for additional_packet in additional_packets:
            if IP in additional_packet:
                if TCP in additional_packet:
                    additional_packet[TCP].dport = additional_ports.pop()
                elif UDP in additional_packet:
                    additional_packet[UDP].dport = additional_ports.pop()

        return additional_packets

    def modify_time(self, additional_packets, vremena_razlika, current_time):
        """
            Modify timestamps of additional packets.

            Args:
                additional_packets (list): A list of additional packets.
                vremena_razlika (list): A list of time differences between packets.
                current_time (float): The current timestamp.

            Returns:
                list: The modified list of additional packets with updated timestamps.
        """

        for additional_packet, time_diff in zip(additional_packets, vremena_razlika):
            additional_packet.time = current_time + (time_diff * 2)
            current_time = current_time + (time_diff * 2)

        return additional_packets

    def scale_attack(self, scale_factor):
        """
            Scale the attack by generating additional packets.

            Args:
                scale_factor (int): The factor by which to scale the attack.

            Returns:
                list: The list of all packets after scaling.
        """

        additional_ports = self.generiraj_portove(scale_factor)
        vremena_razlika = self.vremena_izmedu_paketa(self.victim_packets, scale_factor)
        additional_packets = self.gen_additional_packets(self.victim_packets, scale_factor)
        additional_packets_with_ports = self.modify_ports(additional_packets, additional_ports)

        additional_packets_timestamps = self.modify_time(additional_packets, vremena_razlika,
                                                         self.victim_packets[-1].time)

        all_packets = list()
        all_packets.extend(self.victim_packets)
        all_packets.extend(additional_packets_timestamps)

        all_packets_sorted = sorted(all_packets, key=lambda x: x.time)
        self.victim_packets = all_packets_sorted


    def define_attacker(self):
        """
            Define the attacker by extracting information from the first packet.

            Returns:
                dict: A dictionary containing attacker and victim information.
        """

        attacker_packet = self.packets[0]

        attacker = {
            "Attacker MAC": attacker_packet[Ether].src,
            "Victim MAC": attacker_packet[Ether].dst,
            "Attacker IP": attacker_packet[IP].src,
            "Victim IP": attacker_packet[IP].dst,
            "Attacker port": str(attacker_packet[TCP].sport) if TCP in attacker_packet else str(attacker_packet[UDP].sport) if UDP in attacker_packet else str(attacker_packet[ICMP].sport) if ICMP in attacker_packet else None,
            "Victim port": str(attacker_packet[TCP].dport) if TCP in attacker_packet else str(attacker_packet[UDP].dport) if UDP in attacker_packet else str(attacker_packet[ICMP].dport) if ICMP in attacker_packet else None,
        }

        return attacker

    def razdvoji_pakete(self):
        """
            Separate packets into attacker and victim packets.

            Returns:
                tuple: A tuple containing lists of attacker and victim packets.
        """

        paketi_napadac = []
        paketi_zrtva = []

        for paket in self.packets:
            if IP in paket:
                if paket[IP].src == self.attacker["Attacker IP"]:
                    paketi_napadac.append(paket)
                elif paket[IP].src == self.attacker["Victim IP"]:
                    paketi_zrtva.append(paket)

        return paketi_napadac, paketi_zrtva

    def shift_packet_times(self, start_attack):
        """
        Učitava pcap datoteku, pomiče vremena paketa za vrijednost start_attack
        i sprema modificiranu pcap datoteku na disk.

        Argumenti:
        packets (list): Lista mrežnih paketa
        start_attack (int): Vrijeme u sekundama za pomicanje vremena paketa
        """

        time_first_packet = self.victim_packets[0].time
        start_attack = EDecimal(start_attack)

        # Pomakni vremena paketa
        modified_packets = list()
        for packet in self.victim_packets:
            # Dohvati vrijeme paketa kao datetime objekt
            packet_time = packet.time

            # Pomakni vrijeme za start_attack sekundi
            shifted_time = packet_time + (start_attack - time_first_packet)

            # Ažuriraj vrijeme paketa
            packet.time = shifted_time

            modified_packets.append(packet)

        self.victim_packets = modified_packets
