# Universal Attack Generator

The Universal Attack Generator is a program that generates parameterized network attack traffic based on a given network topology and defined attack scenario. 
The program is designed in such a way that it is possible to control the time when the traffic is generated, its intensity, source and destination, and all other parameters that depend on the topology on which the attack is conducted. 
The topologies are defined in the format used by the CCS simulator.

## Table of contents

- [ How to Install and Run the Project ](#how-to-install-and-run-the-project)
- [ Sequential Diagram ](#sequential-diagram)

## How to Install and Run the Project

Firstly you need to clone repository from GitLab page:
```
git clone https://gitlab.com/VilimP/universal-attack-generator.git
```
After you successfully cloned GitLab repository you need to install requirements:
```
pip install -r requirements.txt
```
For "installation" this is it!

Now you need to run project.
Entry point of the program is main.py.
Program usage is displayed below:
```
usage: main.py [-h] topology_file scenario_file

Generate network attack traffic based on given topology and scenario files.

positional arguments:
  topology_file  The path to the topology file.
  scenario_file  The path to the scenario file.

options:
  -h, --help     show this help message and exit
```

- Topology file needs to be inside ./Topology folder.
- Scenario file needs to be inside ./Scenario folder.

Example of running program:

```
python main.py .\Topology\Global.json .\Scenario\Scenario.yml
```

## Sequential diagram

The program is designed in a modular way so that each part can function independently. The program consists of three main parts:
- Topology Parser
- Scenario Parser
- Attack Generator
- PcapMerger.

After the program is started by the user, it first retrieves the topology, parses it, and converts it into a simpler format that is easier to work with. The program is designed to work with CCS topologies that do not contain network interfaces or IP addresses for nodes. During the conversion to another format, network interfaces and IP addresses are assigned.

The second part of the program is the Scenario Parser, which reads the scenario from the specified file and determines which individual attacks need to be generated. The scenario consists of six phases, each of which can have any number of attacks, or as I call them, tactics. These are the phases of the attack:
- Reconnaissance
- Initial compromise
- Establishing Persistence
- Lateral Movement
- Data exfiltration
- Maintaining Access

After the program has gathered all the necessary information, i.e., it has read the topology and the scenario, the attack generation begins.

The Attack Generator is designed in such a way that the scenario dictates which attack needs to be generated. The generator looks into a local database of small attack samples (pcap files), finds the required file, and reads it. After reading it, if necessary, it modifies certain parameters, scales the attack, and adjusts the attack intensity. This process repeats for each phase or tactic defined in the scenario.

After all attacks have been generated and saved into separate pcap files, the program takes them all and inserts them into a pcap file of normal network traffic, sorted by time.

Link to diagram:
https://sequencediagram.org/index.html#initialData=C4S2BsFMAIFUDsQDdICcDOBDc0CCxhMBjAa2gHFJ41NgB7VAKEYAdNVQiQ35g500rdp26Ze0ALKYQ8IRxBcefACp0WdcHQDmAT2gAFdgKZt5isXwDKRKuxB0DRwaZFK8BYmUrVUtBnNcLAyJMFkk0LWdhBVFxADEQKGZYYwBaAD4pGQAuACUAV3hoFlRtXwBbaAAzBmh0G3g7OgAaaHp1TV1mLPgM1Q7tPUMMNGzHEba1DUHGfundcbT0hKgxy0wUaoZy2mBIABNJgYX6asTIWanOoadUDJ6x3MhgfNQimtQdggOj+Z1u6S9dLWWyoeyLUYQuoNJqMEGNMEOYZLB7QJ4vN7QcAgdB8OhVaC7TzoaAAClxkBY6AAlJNoJEfLQLowehl8IRSBQqDR6Kgxt4eTAiaQSVVSpV6qD7IwVpA2R5OQLfLzHpBMIdaJ5iuxMOVnmhReLoVK6IxNGotqhCQqyDJjQjpfA6HtoHQUFb2VqlX4+dAAMIACzodAE1o5ZFODMFMvO8vDXMZKrRao1NrqupYSSdLrdaHc8e9ScLQptzE9iu5yoYGVlYwA6mCXcKI74qlUFHSWCEwu2klR9gCZBl9N3wqhIr6JBEYE7PthCfBU-HgK320QY1Bh6Op+PIU91WGtV3QugN3L0iPQmOJ6qD7OdjgV5g2x3j1TGNmYLmrZewjub9eJbhiSADuYABtA97zk+L7rr+gF3Ms5z1o2MCnO2jQ4G+0BEHQvCAjIWiQds85iEuWowWuzD7JAuKlDoz57FaKSCDRdF0HoPSMGxK4cdAczXBCTA8fR0Dwk0QncbRvF6OWXiVj6UnsXospKTJwRXv+aBAA

![Description of the image](Resources/Universal%20Attack%20Generator.svg)

## Authors and acknowledgment
Vilim Pagon

Sveučilište u Zagrebu Fakultet elektrotehnike i računarstva

## License
For open source projects, say how it is licensed.

## Project status
Project is in Beta status. Checking for bugs.
