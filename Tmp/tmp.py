from TopologyFormatter import *


topology_file = "../Topology/Global.json"
topology = run_formatter(topology_file)

start_search_node = topology["nodes"][0]

start_node = find_node(topology, "10.0.51.26")
end_node = find_node(topology, "10.0.0.9")

path = find_path(topology, start_node["name"], end_node["name"])

for node in path:
    print(node)
