import json


class Node:
    def __init__(self, name, type, interfaces, connects_to, routing_table):
        self.name = name
        self.type = type
        self.interfaces = interfaces
        self.routing_table = routing_table
        self.connects_to = connects_to
        self.neighbours = list()

    def print_node(self):
        if self.routing_table is not None:
            return (f"{self.name.upper()}\n\ttype: {self.type}\n\tinterfaces:\n\t\t"
                    f"{'\n\t\t'.join(str(interface) for interface in self.interfaces)}\n\trouting table:\n\t\t"
                    f"{'\n\t\t'.join(str(rule) for rule in self.routing_table if self.routing_table is not None)}"
                    f"\n\tconnects to: {self.connects_to}")
        else:
            return (f"{self.name.upper()}\n\ttype: {self.type}\n\tinterfaces:\n\t\t"
                    f"{'\n\t\t'.join(str(interface) for interface in self.interfaces)}"
                    f"\n\tconnects to: {self.connects_to}")


class TopologyParser:
    def __init__(self, file_path):
        self.nodes = self.read_topology(file_path)

        for node in self.nodes:
            print(node.print_node())

    def read_topology(self, file_path):

        nodes_list = list()

        with open(file_path, "r") as file:
            json_data = file.read()
            data = json.loads(json_data)
            nodes = data["nodes"]

            for node in nodes:
                name = node["name"]
                type = node["type"]
                interfaces = node["interfaces"]
                connects_to = node["connects_to"]
                if "routing_table" in node:
                    routing_table = node["routing_table"]
                else:
                    routing_table = None
                nodes_list.append(Node(name, type, interfaces, connects_to, routing_table))

            nodes_list_tmp = nodes_list.copy()

            for node in nodes_list:
                for node_tmp in nodes_list_tmp:
                    if node_tmp.name in node.connects_to:
                        node.neighbours.append(node_tmp)

        return nodes_list

    def in_subnet(self, ip_address, interface):

        # Pretvori IP adrese i subnet maske u liste brojeva
        ip_parts = [int(part) for part in ip_address.split('.')]
        subnet_parts = [int(part) for part in interface["subnet_mask"].split('.')]

        # Pretvori IP adresu sučelja, subnet masku i IP adresu u binarni oblik
        interface_ip_binary = ''.join(format(int(part), '08b') for part in interface["ip_address"].split('.'))
        subnet_binary = ''.join(format(part, '08b') for part in subnet_parts)
        ip_binary = ''.join(format(part, '08b') for part in ip_parts)

        # Izračunaj mrežnu adresu sučelja i za predanu IP adresu
        network_address = ''.join(str(int(interface_ip_binary[i]) & int(subnet_binary[i])) for i in range(32))
        ip_network_address = ''.join(str(int(ip_binary[i]) & int(subnet_binary[i])) for i in range(32))

        # Usporedi mrežne adrese
        return network_address == ip_network_address

    def find_subnet(self, ip_address):
        for node in self.nodes:
            if node.type == "router":
                for interface in node.interfaces:
                    if self.in_subnet(ip_address, interface):
                        print(f"{ip_address} in subnet {interface['ip_address']}/"
                              f"{sum([bin(int(part))[2:].count('1') for part in interface['subnet_mask'].split('.')])}")
                        return node, interface

        print(f"This topology doesn't contain subnet for {ip_address}")


if __name__ == "__main__":
    parser = TopologyParser("../Topology/my_format_json/FER_topology.json")
    print()
    node_router, interface_subnet = parser.find_subnet("10.0.1.5")
