import copy
import time

from scapy.layers.inet import IP, TCP, UDP
from scapy.layers.l2 import Ether
from scapy.utils import rdpcap, wrpcap
from Util import define_attack, generate_pcap, calc_scale_factor
import Attack_generator
import Victim_generator


class Generator:
    def __init__(self, input_file, attack):
        self.packets = rdpcap(input_file)
        self.attacker = self.define_attacker()
        self.attacker_packets, self.victim_packets = self.razdvoji_pakete()
        self.attack = attack

    def modify(self, parameters):
        pass

    def generate_pcap(self, output_file):
        wrpcap(output_file, self.attacker_packets)

    def define_attacker(self):
        attacker_packet = self.packets[0]

        attacker = {
            "Attacker MAC": attacker_packet[Ether].src,
            "Victim MAC": attacker_packet[Ether].dst,
            "Attacker IP": attacker_packet[IP].src,
            "Victim IP": attacker_packet[IP].dst,
            "Attacker port": str(attacker_packet[TCP].sport),
            "Victim port": str(attacker_packet[TCP].dport),
        }

        return attacker

    def razdvoji_pakete(self):
        paketi_napadac = []
        paketi_zrtva = []

        for paket in self.packets:
            if IP in paket:
                if paket[IP].src == self.attacker["Attacker IP"]:
                    paketi_napadac.append(paket)
                elif paket[IP].src == self.attacker["Victim IP"]:
                    paketi_zrtva.append(paket)

        return paketi_napadac, paketi_zrtva

    def generiraj_portove(self, scale_factor):
        # Lista portova koji se koriste u prometu
        postojeci_portovi = set()

        # Pronalazak svih korištenih portova u prometu
        for paket in self.packets:
            if TCP in paket:
                postojeci_portovi.add(paket[TCP].sport)
                postojeci_portovi.add(paket[TCP].dport)
            elif UDP in paket:
                postojeci_portovi.add(paket[UDP].sport)
                postojeci_portovi.add(paket[UDP].dport)
        postojeci_portovi.remove(int(self.attacker["Victim port"]))
        broj_zeljenih_portova = scale_factor * len(postojeci_portovi)

        # Generiranje preostalih portova
        preostali_portovi = set(range(1024, 65536)) - postojeci_portovi

        potrebni_portovi = set()
        for port in preostali_portovi:
            potrebni_portovi.add(port)
            if len(potrebni_portovi) >= broj_zeljenih_portova - len(postojeci_portovi):
                break
        return potrebni_portovi

    def vremena_izmedu_paketa(self, packets, scale_factor):
        vremena = []

        for i in range(1, len(packets)):
            trenutni_paket = packets[i]
            prethodni_paket = packets[i - 1]

            # Dobivanje vremena dolaska za trenutni i prethodni paket
            trenutno_vrijeme = trenutni_paket.time
            prethodno_vrijeme = prethodni_paket.time

            # Izračunavanje razlike u vremenima
            razlika_vremena = trenutno_vrijeme - prethodno_vrijeme

            # Dodavanje razlike u vremenu u listu vremena
            vremena.append(razlika_vremena)

        vremena.append(vremena[-1])

        vremena_mul = copy.deepcopy(vremena)
        for i in range(scale_factor - 2):
            vremena_mul.extend(vremena)

        return vremena_mul

    def scale_attack(self, scale_factor):
        pass

def main(attack_type, duration):
    parameters = {
        # "Attacker MAC": "11:22:33:44:55:66",
        # "Victim MAC": "66:55:44:33:22:11",
        #
        # "Attacker IP": "10.0.0.20",
        # "Victim IP": "10.0.0.30",
        # "TTL": 32,
        # "Protocol": 6,
        #
        # "Attacker port": 1515,
        # "Victim port": 80,
        # "Flags": "S",
        # "Seq Number": 44,
        # "Ack Number": 55,
        # "Window Size": 12,
        #
        # "HTTP": True,
        # "Raw load": b'Bok',
        # "Host": b'www.fer.hr:80',
        # "Http Version": b'HTTP/1.1',
        # "Keep Alive": b'120',
        # "Method": b'GET',
        # "Path": b'/?RLZZYBYPU=JJQF',x
        # "User Agent": b'Mozilla/5.0 (Windows; U; Windows NT 6.1; en; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 3.5.30729)',
        # "fields": {'Accept_Charset': b'ISO-8859-1,utf-8;q=0.7,*;q=0.7', 'Accept_Encoding': b'identity', 'Cache_Control': b'no-cache', 'Connection': b'close', 'Host': b'study.mazebolt.com:80', 'Keep_Alive': b'120', 'Referer': b'http://www.usatoday.com/search/results?q=HBRELQHLF', 'User_Agent': b'Mozilla/5.0 (Windows; U; Windows NT 6.1; en; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 3.5.30729)', 'Method': b'GET', 'Path': b'/?RLZZYBYPU=JJQF', 'Http_Version': b'HTTP/1.1'},
        # "original": b'GET /?RLZZYBYPU=JJQF HTTP/1.1\r\nAccept-Encoding: identity\r\nUser-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.1; en; rv:1.9.1.3) Gecko/20090824 Firefox/3.5.3 (.NET CLR 3.5.30729)\r\nCache-Control: no-cache\r\nAccept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\nReferer: http://www.usatoday.com/search/results?q=HBRELQHLF\r\nKeep-Alive: 120\r\nConnection: close\r\nHost: study.mazebolt.com:80\r\n\r\n',
        #
        # "Queried Domain": b'1033edge.com',
        #
        # "ICMP Type": 8,
    }

    # attack_type = args.attack
    # duration = int(args.duration)
    print("Attack type: ", attack_type)
    print("Attack duration: ", duration)

    input_file, output_file = define_attack(attack_type)

    t_start = time.time()

    attack_generator = Attack_generator.AttackGenerator(input_file, attack_type)
    victim_generator = Victim_generator.VictimGenerator(input_file, attack_type)

    scale_factor = calc_scale_factor(attack_generator.packets, duration)
    print("Scale factor: ", scale_factor)

    attack_packets_generated = attack_generator.modify(parameters)
    victim_packets_generated = victim_generator.modify(parameters)

    scaled_attack_packets = attack_generator.scale_attack(scale_factor)
    scaled_victim_packets = victim_generator.scale_attack(scale_factor)

    generated_packets = list()
    generated_packets.extend(scaled_victim_packets)
    generated_packets.extend(scaled_attack_packets)

    generate_pcap(output_file, generated_packets)

    print(f"Generated {attack_type} attack in {round(time.time() - t_start, 2)}s")
