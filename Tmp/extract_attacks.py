import csv
import re
import string
import time

from scapy.all import *
from scapy.layers.inet import IP, TCP, UDP
import os


def clean_attack_name(attack_name):
    # Remove URLs and special characters (except underscores and hyphens) from the attack name
    attack_name = re.sub(r'\(https?://[\w./]+\)', '', attack_name)
    allowed_chars = string.ascii_letters + string.digits + '_-' + ' '
    attack_name = ''.join(char for char in attack_name if char in allowed_chars)
    return attack_name.strip()


def extract_attacks(csv_file):
    print("Reading CSV file...", end="")
    attacks = []
    with open(csv_file, 'r') as file:
        reader = csv.DictReader(file)
        for row in reader:
            source_ip = row['Source IP']
            source_port = row['Source Port']
            dest_ip = row['Destination IP']
            dest_port = row['Destination Port']
            attack_name = clean_attack_name(row['Attack Name'])
            if attack_name and len(attack_name) <= 120 and not attack_name.startswith('CVE'):
                attack = {
                    'source_ip': source_ip,
                    'source_port': int(source_port),
                    'dest_ip': dest_ip,
                    'dest_port': int(dest_port),
                    'attack_name': attack_name,
                    'type': row['Attack category']
                }
                attacks.append(attack)
        print("\tDone!")
    return attacks


def process_pcap(pcap_file, attacks):
    print("Reading PCAP file...", end="")
    packets = rdpcap(pcap_file)
    print("\tDone!")
    num_packet = 0
    num_packet_confirmed = 0
    packets_to_save = {}
    for packet in packets:
        num_packet += 1
        # if num_packet != (num_packet_confirmed - 1):
        #     print("nesto nije dobro")
        if packet.haslayer(IP) and packet.haslayer(TCP):
            src_ip = packet[IP].src
            dst_ip = packet[IP].dst
            tcp_src_port, tcp_dst_port, udp_src_port, udp_dst_port = 0, 0, 0, 0
            if TCP in packet:
                tcp_src_port = packet[TCP].sport
                tcp_dst_port = packet[TCP].dport
            if UDP in packet:
                udp_src_port = packet[UDP].sport
                udp_dst_port = packet[UDP].dport

            for attack in attacks:
                if ((src_ip == attack['source_ip'] and dst_ip == attack['dest_ip']
                     and (udp_src_port == attack["source_port"] or tcp_src_port == attack["source_port"]))
                        or (src_ip == attack['dest_ip'] and dst_ip == attack['source_ip']
                            and (udp_dst_port == attack["dest_port"] or tcp_dst_port == attack["dest_port"]))):
                    # save_packet(packet, attack['attack_name'])
                    if attack['attack_name'] not in packets_to_save:
                        packets_to_save[attack['attack_name']] = {}
                        packets_to_save[attack['attack_name']]['packets'] = list()
                        packets_to_save[attack['attack_name']]['type'] = attack['type']

                    packets_to_save[attack['attack_name']]['packets'].append(packet)
                    num_packet_confirmed += 1
                    if num_packet % 10000 == 0:
                        print(num_packet, "/", len(packets), f"({num_packet_confirmed})")
                    break
    return packets_to_save


def save_packets(packets):
    # with open(f"../Atks/{attack_name}.pcap", "ab") as f:
    #     f.write(bytes(packet))
    for attack_name, attack_info in packets.items():
        packets = attack_info["packets"]
        attack_type = attack_info["type"]

        # Sort packets by their timestamp
        sorted_packets = sorted(packets, key=lambda pkt: pkt.time)

        # Define the PCAP filename with the provided path format
        pcap_filename = f"../Attack_templates/{attack_type.strip()}/{attack_name}.pcap"

        if os.path.exists(pcap_filename):
            for index in range(1, 100):
                if not os.path.exists(f"../Attack_templates/{attack_type.strip()}/{attack_name}_{index}.pcap"):
                    pcap_filename = f"../Attack_templates/{attack_type.strip()}/{attack_name}_{index}.pcap"
                    break

        # Write sorted packets to the PCAP file
        wrpcap(pcap_filename, sorted_packets)

        # Print confirmation message
        print(f"Packets ({len(sorted_packets)}) for {attack_name} successfully saved to {pcap_filename}")


# Usage example
csv_file = 'NUSW-NB15_GT.csv'
attacks = extract_attacks(csv_file)

for i in [1, 10, 11, 12, 13, 14, 15, 16, 17, 27]:
    for j in [0, 1, 2, 3]:
        t1 = time.time()
        pcap_file = f'{i} - 175.45.176.{j} - filtered.pcap'
        print(f"***** Proccessing file {pcap_file} *****")
        packets_to_save = process_pcap(pcap_file, attacks)
        save_packets(packets_to_save)
        print(f"Packets from {pcap_file} saved in {time.time() - t1}s!")
